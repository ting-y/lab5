﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace unittest.Models
{
    public interface IRepository<T>
    {

        IEnumerable<T> Get();

        T Get(int? id);

        void Create(T model);

        void Update(T model);

        void Delete(int id);
    }
}