﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace unittest.Models
{
    public class CourseRepository : IRepository<Course>, IDisposable
    {

        private SheridanCollege_TY_Model db = new SheridanCollege_TY_Model();
        public void Create(Course course)
        {
            db.Course.Add(course);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            Course course = Get(id);
            db.Course.Remove(course);
            db.SaveChanges();
        }

        public IEnumerable<Course> Get()
        {
            return db.Course.ToList();
        }

        public Course Get(int? id)
        {
            return db.Course.Find(id);
        }

        public void Update(Course course)
        {
            db.Entry(course).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}