﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using unittest.Models;

namespace unittest.Tests.Controllers
{
    public class FakeCoursesController : IRepository<Course>
    {
        public void Create(Course model)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Course> Get()
        {
            List<Course> courses = new List<Course>() {
                new Course() { CourseName = "testCourse", CourseNumber="1010001" },
                new Course() { CourseName = "testCourse2", CourseNumber="1010002" }
            };

            return courses;
        }

        public Course Get(int? id)
        {
            throw new NotImplementedException();
        }

        public void Update(Course model)
        {
            throw new NotImplementedException();
        }
    }
}
