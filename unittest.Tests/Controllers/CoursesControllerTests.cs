﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using unittest.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using unittest.Models.Fakes;
using unittest.Models;
using unittest.Tests.Controllers;
using Microsoft.QualityTools.Testing.Fakes;

namespace unittest.Controllers.Tests
{
    [TestClass()]
    public class CoursesControllerTests
    {
        [TestMethod()]
        public void CoursesControllerTest_Get_courses()
        {
            CoursesController courseCtrl = new CoursesController();
            ViewResult result = courseCtrl.Index() as ViewResult;

            Assert.IsNotNull(result.Model);
        }
        

        [TestMethod()]
        public void Fake_CoursesControllerTest_Get_courses()
        {
            FakeCoursesController fakeCtrl = new FakeCoursesController();
            
            List<Course> result = fakeCtrl.Get().ToList();
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void Stub_CoursesControllerTest_Get_courses()
        {
            StubIRepository<Course> repo = new StubIRepository<Course>();

            repo.Get = () => new List<Course>
            {
                new Course() {  CourseName = "testCourse", CourseNumber="1010001" },
                new Course() {  CourseName = "testCourse", CourseNumber="1010001" },
                new Course() {  CourseName = "testCourse", CourseNumber="1010001" }
            };
            CoursesController controller = new CoursesController(repo);

            //Act
            ViewResult result = controller.Index() as ViewResult;
            List<Course> courses = result.Model as List<Course>;

            Assert.AreEqual(3, courses.Count);
            Assert.AreEqual("testCourse", courses[0].CourseName);
        }


    }
}