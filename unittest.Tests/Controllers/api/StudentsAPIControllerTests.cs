﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using unittest.Controllers.api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using unittest.Models;

namespace unittest.Controllers.api.Tests
{
    [TestClass()]
    public class StudentsAPIControllerTests
    {
        [TestMethod()]
        public void GetStudentTest()
        {
            StudentsAPIController apiCtrl = new StudentsAPIController();
            IQueryable<Student> result = apiCtrl.GetStudent() as IQueryable<Student>;
            Assert.IsNotNull(result.ToList());
        }

    }
}