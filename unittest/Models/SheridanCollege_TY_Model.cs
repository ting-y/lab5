namespace unittest.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class SheridanCollege_TY_Model : DbContext
    {
        public SheridanCollege_TY_Model()
            : base("name=SheridanCollege_TY_Model")
        {
        }

        public virtual DbSet<Course> Course { get; set; }
        public virtual DbSet<Student> Student { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>()
                .HasMany(e => e.Student)
                .WithMany(e => e.Course)
                .Map(m => m.ToTable("CourseStudent"));
        }
    }
}
