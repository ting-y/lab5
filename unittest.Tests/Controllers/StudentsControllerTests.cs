﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using unittest.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using unittest.Models;

namespace unittest.Controllers.Tests
{
    [TestClass()]
    public class StudentsControllerTests
    {

        StudentsController controller = new StudentsController();
        [TestMethod()]
        // TEST GET STUDENTS
        public void StudentsControllerTest()
        {
            
            ViewResult result = controller.Index() as ViewResult;
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        // TEST ADD STUDENT
        public void CreateTest()
        {
            Student student = new Student()
            {
                FirstName = "test",
                LastName = "test",
                GPA = "23"
            };
            ViewResult result = controller.Create(student) as ViewResult;

            Assert.IsNotNull(result.Model);
        }

        [TestMethod()]
        public void DeleteTest()
        {
            // Act
            ViewResult resultDelete = controller.Delete(5) as ViewResult;           
            Assert.IsNotNull(resultDelete);

        }
    }
}