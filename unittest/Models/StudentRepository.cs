﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace unittest.Models
{
    public class StudentRepository : IRepository<Student>, IDisposable
    {
        private SheridanCollege_TY_Model db = new SheridanCollege_TY_Model();

        public void Create(Student student)
        {
            db.Student.Add(student);
            db.SaveChanges();
        }

        public void Delete(int id)
        {
            Student student = Get(id);

            db.Student.Remove(student);
            db.SaveChanges();
        }

        public IEnumerable<Student> Get()
        {
            return db.Student.ToList();
        }

        public Student Get(int? id)
        {
            return db.Student.Find(id);
        }

        public void Update(Student student)
        {
            db.Entry(student).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}